import { createElement } from '../helpers/domHelper';
import { getFighterInfo } from './fighterSelector';
import { fighterDetailsMap } from './fighterSelector';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)
  const fighterDescription = createElement({
    tagName: 'div',
    className:'fighter-preview__description'
  });

  if (fighter) {
    const {name,health,attack,defense} = fighter;
    fighterDescription.innerHTML = `<p>Name: ${name}</p><p>Health: ${health}</p><p>Attack: ${attack}</p><p>Defense: ${defense}</p>`;
    fighterElement.append(fighterDescription);
    fighterElement.append(createFighterImage(fighter));
  }


  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
