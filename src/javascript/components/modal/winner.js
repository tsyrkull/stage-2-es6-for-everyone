import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function

  showModal({title: `${fighter.name} won`, bodyElement: `Congratulations!`, onClose: ()=>{
    location.reload()
    }})
}
