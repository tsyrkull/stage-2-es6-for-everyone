import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const firstFighterHealthIndicator = document.getElementById('left-fighter-indicator');
    const secondFighterHealthIndicator = document.getElementById('right-fighter-indicator');
    const fullFirstFighterHealth = firstFighter.health;
    const fullSecondFighterHealth = secondFighter.health;
    let firstHealthWidth;
    let secondHealthWidth;
    let isBlocked = false;


    document.addEventListener('keydown', (event) => {


      if (event.code === 'KeyA' && !isBlocked) {

        secondFighter.health -= getHitPower(firstFighter);
        secondHealthWidth = secondFighter.health / fullSecondFighterHealth * 100;
        secondFighterHealthIndicator.style.width = `${secondHealthWidth}%`;


      }

      if (event.code === 'KeyJ' && !isBlocked) {
        firstFighter.health -= getHitPower(secondFighter);
        firstHealthWidth = firstFighter.health / fullFirstFighterHealth * 100;
        firstFighterHealthIndicator.style.width = `${firstHealthWidth}%`;

      }

      if (event.code === 'KeyD' || event.code === 'KeyL') {
        if (isBlocked) {
          return;
        }
        isBlocked = true;
      }


      if (firstFighter.health <= 0) {
        resolve(secondFighter);
      }

      if (secondFighter.health <= 0) {
        resolve(firstFighter);
      }


    });

    document.addEventListener('keyup', (event) => {
      if (event.code === 'KeyD' || event.code === 'KeyL') {
        if (!isBlocked) {
          return;
        }
        isBlocked = false;

      }

    });


    function moreThenOneKeys(func, ...codes) {
      let pressed = new Set();
      document.addEventListener('keydown', function(event) {
        pressed.add(event.code);
        for (let code of codes) {
          if (!pressed.has(code)) {
            return;
          }
        }
        pressed.clear();

        func();
      });

      document.addEventListener('keyup', function(event) {
        pressed.delete(event.code);
      });

    }


    moreThenOneKeys(
      throttle(makeCriticalHitFirst, 10000),
      'KeyQ',
      'KeyW',
      'KeyE'
    );

    moreThenOneKeys(
      throttle(makeCriticalHitSecond, 10000),
      'KeyU',
      'KeyI',
      'KeyO'
    );

    function makeCriticalHitFirst() {
      secondFighter.health -= 2 * firstFighter.attack;
      secondHealthWidth = secondFighter.health / fullSecondFighterHealth * 100;
      secondFighterHealthIndicator.style.width = `${secondHealthWidth}%`;

      if (secondFighter.health <= 0) {
        resolve(firstFighter);
      }

    }

    function makeCriticalHitSecond() {
      firstFighter.health -= 2 * secondFighter.attack;
      firstHealthWidth = firstFighter.health / fullFirstFighterHealth * 100;
      firstFighterHealthIndicator.style.width = `${firstHealthWidth}%`;

      if (firstFighter.health <= 0) {
        resolve(secondFighter);
      }

    }

    function throttle(func, timeLimit) {
      let lastTime = 0;
      return function(...args) {
        const firstTime = new Date().getTime();
        if (firstTime - lastTime < timeLimit) {
          return;
        }
        lastTime = firstTime;
        return func(...args);
      };
    }

  });
}

export function getDamage(attacker, defender) {
  // return damage
  return (getHitPower(attacker) - getBlockPower(defender)) > 0 ? (getHitPower(attacker) - getBlockPower(defender)) : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const max = 2;
  const min = 1;
  const attack = fighter.attack;
  const criticalHitChance = Math.random() * (max - min) + min;
  const power = attack * criticalHitChance;

  return power;


}

export function getBlockPower(fighter) {
  // return block power
  const defense = fighter.defense;
  const max = 2;
  const min = 1;
  const dodgeChance = Math.random() * (max - min) + min;
  const power = defense * dodgeChance;

  return power;
}

